SILICON LABS GECKO SDK LICENSING:

All of the files in this subtree are subject to their original license from
Silicon Labs. All of the licenses are permissive open source.

Please examine the individual files to see the applied license.

Almost all of the files use the Zlib license:

https://opensource.org/licenses/Zlib

A few of the files (CMSIS) use the Apache-2.0 license:

https://www.apache.org/licenses/LICENSE-2.0.txt
