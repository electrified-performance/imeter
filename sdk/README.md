Silicon Labs Gecko SDK
======================

The files in this subdirectory were taken from an installed distribution
of Silicon Labs Simplicity Studio.

https://www.silabs.com/products/development-tools/software/simplicity-studio

The SDK is huge and this only contains the files I need for my project.

**All of the files under this subtree are subject to their original license
from Silicon Labs.**

See the LICENSE file for more details.

At the time of this writing, this is using version 2.7 of the Gecko SDK.

