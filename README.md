Current Meter Project (imeter)
=====================

This project is a current meter using a Silicon Labs [EFM32WG Board](
https://www.silabs.com/products/development-tools/mcu/32-bit/efm32-wonder-gecko-starter-kit)
and a [LEM HASS 300-S current probe](https://www.lem.com/en/hass-300s).

![Wonder Gecko Starter Kit Image](doc/wonder-gecko-starter-kit.jpg)

This is meant for lab or garage use, not for production. I chose the EFM32WG
board simply because I had one and it has a nice big LCD display.

About Silicon Labs SDK Files
----------------------------

This project uses files from the Silicon Labs Gecko SDK. I copied the files
I needed into the `sdk` subdirectory, unchanged from the Simplicity Studio
installation. Please see the README and LICENSE files in the `sdk`
subdirectory.

Building
--------

For this project I used a Makefile to directly invoke the compiler instead
of using Simplicity Studio. To build this you need an environment that can
run GNU Make or similar, and the [GNU Arm Embedded Toolchain](
https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).

    $ cd build
    $ make clean
    $ make

Resulting executable is in `build/exe/imeter.hex`. Other forms of the binary
are also available if needed (.bin, .axf).

NOTE: warnings are turned up high and the build will fail with error if there
are any warnings.  You can disable this when invoking make like this:

    $ make WARNERROR=

Load to Device (Flashing)
-------------------------

I use _Simplicity Commander_ from the command line to flash the program to the
board. You can find a standalone installer about halfway down on this page:

https://www.silabs.com/products/mcu/programming-options

Simplicity Commander is also installed as part of Simplicity Studio so if you
dig through your installation you can find it in there. On a Mac installation
you can find the command line version here:

    /Applications/Commander.app/Contents/MacOS/commander

If you have the EFM32WG board attached via the debug USB port, then you can
flash the program with something like this:

    $ cd build
    $ /opt/commander/commander flash exe/imeter.hex

Debugging
---------

It is a simple program and probably doesn't need a debugger. But just in case,
you can use GDB with [Segger's JLink GDB Server](
https://www.segger.com/products/debug-probes/j-link/tools/j-link-gdb-server/about-j-link-gdb-server/).

First, start the JLink GDB Server. You may have to configure your board type,
but JLink can talk to the EFM32WG board. Once the server is running and shows
attachment, you can do the following commands to start a debug session.

    $ cd build
    $ arm-none-eabi-gdb exe/imeter.axf
    GNU gdb (GNU Tools for ARM Embedded Processors 6-2017-q2-update) 7.12.1.20170417-git
    Copyright (C) 2017 Free Software Foundation, Inc.
    License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
    (blather)
    Reading symbols from exe/imeter.axf...done.
    (gdb) target remote localhost:2331
    Remote debugging using localhost:2331
    0x0000050c in Delay (dlyTicks=100) at ../src/imeter.c:28

Now you can use GDB debug commands. You can restart the program like this:

    (gdb) monitor reset

There are probably other debuggers you can use too if command line GDB is not
your thing.

Running
-------

THIS VERSION SHOWS UNCALIBRATED ADC CH7 VOLTAGE ON THE LCD.

When the program is "flashed" it should start running on its own. You can
restart it by pressing the reset button. Once the program is flashed, you can
power the board from any USB source such as a battery pack or wall wart with
USB cable.

When the program is running, the measured current will show on the display.
