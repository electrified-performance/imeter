/******************************************************************************
 *
 * imeter.c - Current meter using EFM32WG and LEM HASS 300-S
 *
 * Copyright (c) 2020 Joseph Kroesche
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *****************************************************************************/

/**
 * CURRENT STATE OF PROGRAM:
 * * reads ADC voltage and shows the value on the LCD
 */

#include <stdio.h>

// emlib
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "em_adc.h"

// bsp
#include "drivers/retargetserial.h"
#include "drivers/segmentlcd.h"

static volatile uint32_t msTicks = 0;

/**
 * Maintains systick counter for time delays.
 */
void
SysTick_Handler(void)
{
    msTicks++;
}

/**
 * Time delay using a wait loop. Not power efficient.
 *
 * @param dlyTicks length of delay in milliseconds.
 */
void
Delay(uint32_t dlyTicks)
{
    uint32_t curTicks;

    curTicks = msTicks;
    while ((msTicks - curTicks) < dlyTicks)
        ;
}

/**
 * Initialize the A/D converter for measuring voltage from the current probe.
 *
 * Setup:
 *
 * - ADC channel 7 (pin 17 on WG expansion header)
 * - single-ended
 * - oversample by 16 to try and reduce noise
 * - reference is VDD which is about 3.3V
 */
void
initADC(void)
{
    // Enable ADC0 clock
    CMU_ClockEnable(cmuClock_ADC0, true);

    // common init
    ADC_Init_TypeDef init = ADC_INIT_DEFAULT;
    init.prescale = ADC_PrescaleCalc(13000000, 0);  // max ADC clock
    init.timebase = ADC_TimebaseCalc(0);
    init.ovsRateSel = adcOvsRateSel16;              // oversample x16
    ADC_Init(ADC0, &init);

    // single init
    ADC_InitSingle_TypeDef single = ADC_INITSINGLE_DEFAULT;
    single.diff = false;                            // single-ended
    single.reference = adcRefVDD;                   // ref is VDD 3.3V
    single.resolution = adcResOVS;                  // use oversample
    single.input = adcSingleInputCh7;               // channel 7
    ADC_InitSingle(ADC0, &single); 
}

/**
 * Make the "wheel" on the display turn by one step.
 *
 * Call this at a regular interval to make it appear to be spinning.
 */
static void
turnWheel(void)
{
    static uint32_t wheel = 0;
    SegmentLCD_ARing(wheel, 0);
    ++wheel;
    wheel &= 7;
    SegmentLCD_ARing(wheel, 1);
}

#if 0
/**
 * Show current value on the LCD display.
 *
 * @param ma the signed current in milliamps
 *
 * The current will be shown on the display as amps plus single digit of
 * tenths of amps. ex: "123.4 A". A minus sign will be shown if the value
 * is negative.
 */
static void
displayCurrent(int32_t ma)
{
    static char buf[8];
    uint32_t isNeg = 0;
    if (ma < 0)
    {
        isNeg = 1;
        ma = -ma;
    }
    // Adjust value to be units of tenths of an Ampere. This leaves one
    // digit after the decimal. Set the decimal point on the display so
    // it looks right to the user
    ma /= 100;
    snprintf(buf, sizeof(buf), "%04lu A", ma);
    SegmentLCD_Write(buf);
    SegmentLCD_Symbol(LCD_SYMBOL_MINUS, isNeg);
    SegmentLCD_Symbol(LCD_SYMBOL_DP4, 1);
}
#endif

/**
 * Show voltage value on the LCD display.
 *
 * @param mv the signed voltage in millivolts
 *
 * The voltage will be shown on the display as volts plus three digits of
 * millivolts. ex: "1.234 V". A minus sign will be shown if the value
 * is negative.
 */
static void
displayVoltage(int32_t mv)
{
    static char buf[8];
    uint32_t isNeg = 0;
    if (mv < 0)
    {
        isNeg = 1;
        mv = -mv;
    }
    snprintf(buf, sizeof(buf), "%04lu V", mv);
    SegmentLCD_Write(buf);
    SegmentLCD_Symbol(LCD_SYMBOL_MINUS, isNeg);
    SegmentLCD_Symbol(LCD_SYMBOL_DP2, 1);
}

/**
 * MAIN
 */
int main(int argc, char *argv[])
{
    /* Chip errata */
    CHIP_Init();

    /* Setup SysTick Timer for 1 msec interrupts  */
    if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000))
        while (1)
            ;

    RETARGET_SerialInit();
    RETARGET_SerialCrLf(1);

    printf("Hello, world!\n");

    SegmentLCD_Init(false);
    SegmentLCD_AllOff();

    initADC();

    while(1)
    {
        ADC_Start(ADC0, adcStartSingle);
        Delay(100);
        uint32_t sample = ADC_DataSingleGet(ADC0);
        int32_t milliv = (sample * 3260) / 65535;
        SegmentLCD_Number(sample);
        displayVoltage(milliv);
        turnWheel();
    }

    return 0;
}
